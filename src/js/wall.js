class Wall {
  constructor(game) {
    this.game = game;
    this.walls = [];
    this.settings = {
      rowCount: 16,
      minBrickSize: 4,
      maxBrickSize: 10,
      wallName: 'wall-data',
    };
    this.id = 0;
  }
  
  init() {
    this.generateWalls();
    this.printWalls();
  }
  
  generateWalls(end = false) {
    var coordRow = 0;
    var coordCol = 0;
    var row = [];
    while(true) {
      var brick = this.getRandomBrick(coordRow, coordCol);
      row.push(brick); //hozzaadunk egy elemet egy sorhoz
      this.id++;
      coordCol += brick.length;
      if(coordCol + this.settings.maxBrickSize > this.game.settings.map.map_with) {
        var lastLength = this.game.settings.map.map_with - (coordCol);
        var lastBrick = this.getFixSizedBrick(coordRow,coordCol,lastLength);
        
        coordRow++;
        coordCol = 0;
        row.push(lastBrick);
        this.id++;
        if(!end)
          this.walls.push(row); //hozzaadunk egy sort
        else {
          this.walls.unshift(row);
        }
        if(coordRow >= this.settings.rowCount - 1 || end) {
          break;
        }
        var row = [];
      } 
    } 
  }
  
  printWalls() {
    for( var i in this.walls) {
      for(var j in this.walls[i]) {
        //console.log(this.walls[i][j]);
        var brick = this.walls[i][j];
        for(var col=brick.coord[1]; col<brick.coord[1]+brick.length; ++col) {
          var elem = this.game.getCell(col, brick.coord[0]);
          //console.log(elem);
          elem.style.backgroundColor = brick.color;
          elem.setAttribute(this.settings.wallName, brick.id);
        }
        
      }
    }
  }
  
  moveDownWalls() {
    this.clearWalls();
    
    this.wallschangeCoords();
    this.generateWalls(true);
    this.printWalls();
  }
  
  wallschangeCoords() {
    for( var i in this.walls) {
      for(var j in this.walls[i]) {
        //console.log(this.walls[i][j]);
        var brick = this.walls[i][j];
        brick.coord[0]++;
      }
    }
  }
  
  clearWalls() {
    var bricks = this.game.gameBody.querySelectorAll('[' + this.settings.wallName + ']');
    //console.log(bricks);
    for(var i=0; i<bricks.length; ++i) {
      bricks[i].removeAttribute(this.settings.wallName);
      bricks[i].removeAttribute('style');
    }
  }
  
  getRandomBrick(coordRow,coordCol) {
      var brick = {};
      brick.length = this.getRandomNumber(this.settings.minBrickSize, this.settings.maxBrickSize);
      brick.color = this.getRandomColor();
      brick.coord = [coordRow, coordCol]; //baloldali kezdo koordinata
      brick.id = this.id;
      return brick;
  }
  
  deleteOneBrick(id) {
    var bricks = this.game.gameBody.querySelectorAll('[' + this.settings.wallName +'='+'"'+id+ '"]');
    //console.log(bricks);
    for(var i=0; i<bricks.length; ++i) {
      bricks[i].removeAttribute(this.settings.wallName);
      bricks[i].removeAttribute('style');
    }
  }
  
  deleteOneBrickFromWall(id) {
    for( var i in this.walls) {
      for(var j in this.walls[i]) {
        if(this.walls[i][j].id == id) {
          this.walls[i].splice(j,1);
          return;
        }
      }
    }
  }
  
  
  getFixSizedBrick(coordRow,coordCol,length) {
      var brick = {};
      brick.length = length;
      brick.color = this.getRandomColor();
      brick.coord = [coordRow, coordCol]; //baloldali kezdo koordinata
      brick.id = this.id;
      return brick;
  }
  
  getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }
  
  getRandomNumber(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
  }
  
}