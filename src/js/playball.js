class PlayBall {
    constructor(mainContainer) {
        this.started = false;
        this.wall = new Wall(this);
        this.playerPositions = {};
        this.ballPositions = {
            direction : {
                x:1,
                y:-1
            }
        };
        this.mainContainer = mainContainer;
        this.elementGenerator = new GenerateElement();
        this.settings = {
            map : {
                map_with : 45,
                map_height : 35
            },
            classes : {
                map_body_class : 'game-body',
                map_cell_class_name : 'cell',
                player_class: 'player',
                ball_class: 'ball'
            },
            player : {
                length : 10,
                cell_with : 15,
                wall_penetration: 1
            },
            difficulty: 3000
            
        }; 
        this.init();
    }
    
    init() {
        this.createMap();
        this.createPlayer();
        this.attacheEvents();
        this.wall.init();
    }
    
    createMap() {
        this.gameBody = this.elementGenerator.createElement('div');
        this.gameBody.className += this.settings.classes.map_body_class;
        for(var row = 0; row < this.settings.map.map_height; row++) {
            var row_element = this.elementGenerator.createElement('div');
            for(var coll = 0; coll < this.settings.map.map_with; coll++) {
                var coll_element = this.elementGenerator.createElement('div');
                coll_element.className += this.settings.classes.map_cell_class_name;
                row_element.appendChild(coll_element);
            }
            this.gameBody.appendChild(row_element);
        }
        
        this.mainContainer.appendChild(this.gameBody);
    }

    attacheEvents() {
        var self = this;
        document.onmousemove = function(e){
            var x = e.clientX;
            var y = e.clientY;

            self.movePlayer(x);
        };
        
        document.addEventListener('click', function(e){
            if(!self.started)
                self.run();
        });
    }
    
    movePlayer(mouseX) {
        //var leftCell = this.getCell(0, this.settings.map.map_height - 1);
        //var map_left_posionion = leftCell.getBoundingClientRect().left;
        this.calculatePlayerPosition(mouseX);   
        this.clearPlayer();
        this.printPlayer();
    }
    
    calculatePlayerPosition(mouseX) {
        var player_mid_cell = parseInt(mouseX/this.settings.player.cell_with);
        var pos_x = player_mid_cell  - parseInt(this.settings.player.length/2);
        
        if(pos_x < 0)
            this.playerPositions.x = 0;
        else if(pos_x > this.settings.map.map_with - this.settings.player.length -1 ) {
            this.playerPositions.x = this.settings.map.map_with 
                                     - this.settings.player.length -1;
        }
        else {
            this.playerPositions.x = pos_x;
        }
    }
    
    getCell(coll, row) {
        return this.gameBody.children[row].children[coll];
    }
    
    createPlayer() {
        var positions = {};
        this.playerPositions.x = parseInt(this.settings.map.map_with/2)
        - parseInt(this.settings.player.length/2);
        this.playerPositions.y = this.settings.map.map_height - 1;
        this.printPlayer();
    }
    
    printPlayer(positions = this.playerPositions) {
        for(var x = positions.x; x <= positions.x + this.settings.player.length; x++) {
            var cell = this.getCell(x, positions.y);
            cell.className += " " + this.settings.classes.player_class;
        }
        if (!this.started){
            this.createBall()
        }
    }
    
    clearPlayer() {
        var flag = 0;
        var player = this.gameBody.getElementsByClassName(this.settings.classes.player_class);
        
        for(var i = 0; i < player.length; i++) {
            flag = 1;
            player[i].classList.remove(this.settings.classes.player_class);    
        }

        if(flag === 1 && player.length > 0) {
            this.clearPlayer();
        }
    }
    
    createBall() {
        this.clearBall();
        this.ballPositions.x = this.playerPositions.x + parseInt(this.settings.player.length/2);
        this.ballPositions.y = this.playerPositions.y - 1;
        this.printBall();
    }
    
    clearBall() {
        var ball = this.gameBody.getElementsByClassName(this.settings.classes.ball_class);
        if (ball.length > 0)
            ball[0].classList.remove(this.settings.classes.ball_class);
    }
    
    printBall(){
        var cell = this.getCell(this.ballPositions.x , this.ballPositions.y)
        cell.className += " " + this.settings.classes.ball_class;
    }
    
    getRandomNumber(limit, min = 0) {
        return Math.floor(Math.random() * (limit - min)) + min;
    }

    moveBall() {
        
        this.ballPositions.x += this.ballPositions.direction.x;
        this.ballPositions.y += this.ballPositions.direction.y;    
        
        if(this.ballPositions.x == 0 || this.ballPositions.x == this.settings.map.map_with -1) {
            this.ballPositions.direction.x *= -1;
        }
      
        if(this.ballPositions.y == 0 || this.ballPositions.y == this.settings.map.map_height -1) {
            this.ballPositions.direction.y *= -1;
        }
        
        if(!this.check()) {
            return false;
        }
        
        this.clearBall();
        this.printBall();
        
        return true;
    }
    
    check() {
        this.print_ball = true;
        if(this.isGameOver()) {
            alert('GAME OVER');
            return false;
        }
        
        this.checkIsWall();
        
        return true;
    }
    
    isGameOver() {
        if(this.ballPositions.y == this.settings.map.map_height - 1) {
            if(this.isPlayer()) {           
                this.ballPositions.x += this.ballPositions.direction.x;
                this.ballPositions.y += this.ballPositions.direction.y;
                return false;
            }
            
            this.started = false;
            return true;
        }
        
        return false;
    }
    
    checkIsWall() {
        if(this.settings.player.wall_penetration) {
            this.checkBallNextPosition();                         
        }
        else {
            this.isWall(this.ballPositions.x, this.ballPositions.y);
        }
    }
    
    isWall(x, y) {
        var cell = this.getCell(x, y);
        var wall_id = cell.getAttribute(this.wall.settings.wallName);
        if(wall_id) {
           this.wall.deleteOneBrick(wall_id);
           this.wall.deleteOneBrickFromWall(wall_id);
           
           return true;
        }
        
        return false;
    }
    
    checkBallNextPosition() {
        //up right

        if(this.ballPositions.direction.x == 1 && this.ballPositions.direction.y == -1) {
           //up
           var changed = false;
           if(this.isWall(this.ballPositions.x, this.ballPositions.y -1)) {
                this.ballPositions.direction.y *= -1;
                changed = true; 
           }
           
           //cell_right
           if(this.isWall(this.ballPositions.x + 1, this.ballPositions.y)) {
                this.ballPositions.direction.x *= -1;
                changed = true;
           }
           
           //cell_next
           if(this.isWall(this.ballPositions.x + 1, this.ballPositions.y -1) && !changed) {
                this.ballPositions.direction.y *= -1;
                this.ballPositions.direction.x *= -1;
           }
        }
    
        //up left

        if(this.ballPositions.direction.x == -1 && this.ballPositions.direction.y == -1) {
           //up
           var changed = false;
           if(this.isWall(this.ballPositions.x, this.ballPositions.y -1)) {
                this.ballPositions.direction.y *= -1;
                changed = true; 
           }
           
           //cell_left
           if(this.isWall(this.ballPositions.x - 1, this.ballPositions.y)) {
                this.ballPositions.direction.x *= -1;
                changed = true;
           }
           
           //cell_next
           if(this.isWall(this.ballPositions.x -1, this.ballPositions.y -1) && !changed) {
                this.ballPositions.direction.y *= -1;
                this.ballPositions.direction.x *= -1;
           }
        }
        
        //down right
        
        if(this.ballPositions.direction.x == 1 && this.ballPositions.direction.y == 1) {
           //down
           var changed = false;
           if(this.isWall(this.ballPositions.x, this.ballPositions.y + 1)) {
                this.ballPositions.direction.y *= -1;
                changed = true; 
           }
           
           //cell_right
           if(this.isWall(this.ballPositions.x + 1, this.ballPositions.y)) {
                this.ballPositions.direction.x *= -1;
                changed = true;
           }
           
           //cell_next
           if(this.isWall(this.ballPositions.x + 1, this.ballPositions.y + 1) && !changed) {
                this.ballPositions.direction.y *= -1;
                this.ballPositions.direction.x *= -1;
           }
        }
    
        //down left
    
        if(this.ballPositions.direction.x == -1 && this.ballPositions.direction.y == 1) {
           //down
           var changed = false;
           if(this.isWall(this.ballPositions.x, this.ballPositions.y + 1)) {
                this.ballPositions.direction.y *= -1;
                changed = true; 
           }
           
           //cell_left
           if(this.isWall(this.ballPositions.x - 1, this.ballPositions.y)) {
                this.ballPositions.direction.x *= -1;
                changed = true;
           }
           
           //cell_next
           if(this.isWall(this.ballPositions.x - 1, this.ballPositions.y + 1) && !changed) {
                this.ballPositions.direction.y *= -1;
                this.ballPositions.direction.x *= -1;
           }
        }
    
    
    }
    
    isPlayer() {
        var cell = this.getCell(this.ballPositions.x, this.ballPositions.y);
        //console.log(cell);
        if(cell.classList.contains(this.settings.classes.player_class))
            return true;
        
        return false;
    }
    
    run() {
        var self = this;
        if(!this.started) {
            this.started = true;
            this.moveWalls();
        }
    
        setTimeout(function() {
            if(self.moveBall())
                self.run();
        }, 50)
    }
    
    moveWalls() {
        var self = this;
        setTimeout(function() {
            self.wall.moveDownWalls();
            if(self.started)
                self.moveWalls();
        }, this.settings.difficulty);
    }
}

class GenerateElement {
    createElement(tag, text = '') {
        var el = document.createElement(tag);
        
        if(text.length > 0) {
            text = document.createTextNode(text);
            el.appendChild(text);
        }
        
        return el;
    }
    
    changeStyle(element, style_key, style_value) {
        element.style[style_key] = style_value;
    }
}

var container =  document.getElementById('main-container');
new PlayBall(container);

 